/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ge.med.medtestrest.ejb;

import com.ge.med.medtestrest.model.TestCase;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author kpolak
 */
@Stateless(name = "testCasesSessionBean")
public class TestCasesSessionBean {

    @PersistenceContext(unitName = "medtestdb")
    EntityManager em;

    public List<TestCase> getAllTestCases() {
        Query q = em.createNamedQuery("TestCase.findAll");
        List<TestCase> testCases = q.getResultList();
        return testCases;
    }

    public List<TestCase> getTestCasesWithSuffix(String suffix) {
      Query q = em.createNamedQuery("TestCase.findAll");
      List<TestCase> testCases = q.getResultList();
      return testCases;
    }
}
