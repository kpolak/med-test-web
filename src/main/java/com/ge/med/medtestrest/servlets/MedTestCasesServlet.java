package com.ge.med.medtestrest.servlets;

/**
 * Created by kpolak on 06/02/14.
 */

import com.demo.Data;
import com.demo.JqGridData;
import com.demo.Person;
import com.ge.med.medtestrest.ejb.TestCasesSessionBean;
import com.ge.med.medtestrest.model.TestCase;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "MedTestCasesServlet", urlPatterns = {"/MedTestCasesServlet"})
public class MedTestCasesServlet extends HttpServlet {

  private static final long serialVersionUID = 1L;

  @EJB
  TestCasesSessionBean testCasesSessionBean;

  public MedTestCasesServlet() {

  }

  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String testCaseSuffix = request.getParameter("tcSuffix") ;
    // TODO service empty suffix?
    if(testCaseSuffix == null || testCaseSuffix.length() == 0) {
      testCaseSuffix = "ZZZZZ";
    }
    System.out.println("testCaseSuffix: " + testCaseSuffix);
//    List<Person> personList = new Data().getData(postcode);

    List<TestCase> testCases = testCasesSessionBean.getAllTestCases();


    List<Person> personList = new Data().getData("BS1 8QT");

    if (testCaseSuffix != null) {
//      personList.add(new Person("Janek", testCaseSuffix, "324 Regent Street, Bristol"));
      personList.add(new Person("Marek", testCaseSuffix, "" + testCases.size()));
    }


    int totalNumberOfPages = personList.size() / 10;
    int currentPageNumber = 1;
    int totalNumberOfRecords = personList.size();

    JqGridData<Person> gridData = new JqGridData<Person>(totalNumberOfPages, currentPageNumber, totalNumberOfRecords, personList);
//    JqGridData<TestCase> gridData = new JqGridData<TestCase>(totalNumberOfPages, currentPageNumber, totalNumberOfRecords, testCases);
//    System.out.println("Grid Data: " + gridData.getJsonString());
    response.getWriter().write(gridData.getJsonString());
  }

  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

  }
}