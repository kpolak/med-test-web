/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ge.med.medtestrest.servlets;

import com.ge.med.medtestrest.ejb.TestCasesSessionBean;
import com.ge.med.medtestrest.model.TestCase;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.joda.time.DateTime;

/**
 *
 * @author kpolak
 */
@WebServlet(name = "CountTCServlet", urlPatterns = {"/CountTCServlet"})
public class CountTCServlet extends HttpServlet {

    @EJB
    TestCasesSessionBean testCasesSessionBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CountTCServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CountTCServlet at " + request.getContextPath() + "</h1>");

            int count = 0;
            HttpSession session = request.getSession();
            if (session.getAttribute("count") != null) {
                count = (Integer)session.getAttribute("count");
            }
            session.setAttribute("count", ++count);
            out.println("<h4>Timestamp: " + DateTime.now() + "<br>");
            out.println("<h4>Counter: " + session.getAttribute("count") + "<br></h4>");
        
            List<TestCase> testCases = testCasesSessionBean.getAllTestCases();
            
            if (testCases == null) {
                out.println("<h3>TestCase is null</h1>");
            } else {
                out.println("<h3>TestCase: " + testCases.get(0).toString() + "</h1>");
            }

            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
