/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ge.med.medtestrest.model.controller;

import java.io.Serializable;
//import com.ge.med.medtestrest.model.ProtocolTestCases;
//import com.ge.med.medtestrest.model.TestCases;


/**
 *
 * @author kpolak
 */
public class TestCasesJpaController implements Serializable {
/*
    public TestCasesJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TestCases testCases) throws PreexistingEntityException, RollbackFailureException, Exception {
        if (testCases.getProtocolTestCasesCollection() == null) {
            testCases.setProtocolTestCasesCollection(new ArrayList<ProtocolTestCases>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Collection<ProtocolTestCases> attachedProtocolTestCasesCollection = new ArrayList<ProtocolTestCases>();
            for (ProtocolTestCases protocolTestCasesCollectionProtocolTestCasesToAttach : testCases.getProtocolTestCasesCollection()) {
                protocolTestCasesCollectionProtocolTestCasesToAttach = em.getReference(protocolTestCasesCollectionProtocolTestCasesToAttach.getClass(), protocolTestCasesCollectionProtocolTestCasesToAttach.getId());
                attachedProtocolTestCasesCollection.add(protocolTestCasesCollectionProtocolTestCasesToAttach);
            }
            testCases.setProtocolTestCasesCollection(attachedProtocolTestCasesCollection);
            em.persist(testCases);
            for (ProtocolTestCases protocolTestCasesCollectionProtocolTestCases : testCases.getProtocolTestCasesCollection()) {
                TestCases oldTestCodeOfProtocolTestCasesCollectionProtocolTestCases = protocolTestCasesCollectionProtocolTestCases.getTestCode();
                protocolTestCasesCollectionProtocolTestCases.setTestCode(testCases);
                protocolTestCasesCollectionProtocolTestCases = em.merge(protocolTestCasesCollectionProtocolTestCases);
                if (oldTestCodeOfProtocolTestCasesCollectionProtocolTestCases != null) {
                    oldTestCodeOfProtocolTestCasesCollectionProtocolTestCases.getProtocolTestCasesCollection().remove(protocolTestCasesCollectionProtocolTestCases);
                    oldTestCodeOfProtocolTestCasesCollectionProtocolTestCases = em.merge(oldTestCodeOfProtocolTestCasesCollectionProtocolTestCases);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findTestCases(testCases.getTestCode()) != null) {
                throw new PreexistingEntityException("TestCases " + testCases + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TestCases testCases) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            TestCases persistentTestCases = em.find(TestCases.class, testCases.getTestCode());
            Collection<ProtocolTestCases> protocolTestCasesCollectionOld = persistentTestCases.getProtocolTestCasesCollection();
            Collection<ProtocolTestCases> protocolTestCasesCollectionNew = testCases.getProtocolTestCasesCollection();
            Collection<ProtocolTestCases> attachedProtocolTestCasesCollectionNew = new ArrayList<ProtocolTestCases>();
            for (ProtocolTestCases protocolTestCasesCollectionNewProtocolTestCasesToAttach : protocolTestCasesCollectionNew) {
                protocolTestCasesCollectionNewProtocolTestCasesToAttach = em.getReference(protocolTestCasesCollectionNewProtocolTestCasesToAttach.getClass(), protocolTestCasesCollectionNewProtocolTestCasesToAttach.getId());
                attachedProtocolTestCasesCollectionNew.add(protocolTestCasesCollectionNewProtocolTestCasesToAttach);
            }
            protocolTestCasesCollectionNew = attachedProtocolTestCasesCollectionNew;
            testCases.setProtocolTestCasesCollection(protocolTestCasesCollectionNew);
            testCases = em.merge(testCases);
            for (ProtocolTestCases protocolTestCasesCollectionOldProtocolTestCases : protocolTestCasesCollectionOld) {
                if (!protocolTestCasesCollectionNew.contains(protocolTestCasesCollectionOldProtocolTestCases)) {
                    protocolTestCasesCollectionOldProtocolTestCases.setTestCode(null);
                    protocolTestCasesCollectionOldProtocolTestCases = em.merge(protocolTestCasesCollectionOldProtocolTestCases);
                }
            }
            for (ProtocolTestCases protocolTestCasesCollectionNewProtocolTestCases : protocolTestCasesCollectionNew) {
                if (!protocolTestCasesCollectionOld.contains(protocolTestCasesCollectionNewProtocolTestCases)) {
                    TestCases oldTestCodeOfProtocolTestCasesCollectionNewProtocolTestCases = protocolTestCasesCollectionNewProtocolTestCases.getTestCode();
                    protocolTestCasesCollectionNewProtocolTestCases.setTestCode(testCases);
                    protocolTestCasesCollectionNewProtocolTestCases = em.merge(protocolTestCasesCollectionNewProtocolTestCases);
                    if (oldTestCodeOfProtocolTestCasesCollectionNewProtocolTestCases != null && !oldTestCodeOfProtocolTestCasesCollectionNewProtocolTestCases.equals(testCases)) {
                        oldTestCodeOfProtocolTestCasesCollectionNewProtocolTestCases.getProtocolTestCasesCollection().remove(protocolTestCasesCollectionNewProtocolTestCases);
                        oldTestCodeOfProtocolTestCasesCollectionNewProtocolTestCases = em.merge(oldTestCodeOfProtocolTestCasesCollectionNewProtocolTestCases);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = testCases.getTestCode();
                if (findTestCases(id) == null) {
                    throw new NonexistentEntityException("The testCases with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            TestCases testCases;
            try {
                testCases = em.getReference(TestCases.class, id);
                testCases.getTestCode();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The testCases with id " + id + " no longer exists.", enfe);
            }
            Collection<ProtocolTestCases> protocolTestCasesCollection = testCases.getProtocolTestCasesCollection();
            for (ProtocolTestCases protocolTestCasesCollectionProtocolTestCases : protocolTestCasesCollection) {
                protocolTestCasesCollectionProtocolTestCases.setTestCode(null);
                protocolTestCasesCollectionProtocolTestCases = em.merge(protocolTestCasesCollectionProtocolTestCases);
            }
            em.remove(testCases);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TestCases> findTestCasesEntities() {
        return findTestCasesEntities(true, -1, -1);
    }

    public List<TestCases> findTestCasesEntities(int maxResults, int firstResult) {
        return findTestCasesEntities(false, maxResults, firstResult);
    }

    private List<TestCases> findTestCasesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from TestCases as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TestCases findTestCases(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TestCases.class, id);
        } finally {
            em.close();
        }
    }

    public int getTestCasesCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from TestCases as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
*/    
}
