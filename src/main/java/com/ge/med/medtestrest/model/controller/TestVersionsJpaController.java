/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ge.med.medtestrest.model.controller;

import java.io.Serializable;
//import com.ge.med.medtestrest.model.TestVersions;
//import com.ge.med.medtestrest.model.Protocols;


/**
 *
 * @author kpolak
 */
public class TestVersionsJpaController implements Serializable {
/*
    public TestVersionsJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TestVersions testVersions) throws PreexistingEntityException, RollbackFailureException, Exception {
        if (testVersions.getTestVersionsCollection() == null) {
            testVersions.setTestVersionsCollection(new ArrayList<TestVersions>());
        }
        if (testVersions.getProtocolsCollection() == null) {
            testVersions.setProtocolsCollection(new ArrayList<Protocols>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            TestVersions parentVersion = testVersions.getParentVersion();
            if (parentVersion != null) {
                parentVersion = em.getReference(parentVersion.getClass(), parentVersion.getVersionName());
                testVersions.setParentVersion(parentVersion);
            }
            Collection<TestVersions> attachedTestVersionsCollection = new ArrayList<TestVersions>();
            for (TestVersions testVersionsCollectionTestVersionsToAttach : testVersions.getTestVersionsCollection()) {
                testVersionsCollectionTestVersionsToAttach = em.getReference(testVersionsCollectionTestVersionsToAttach.getClass(), testVersionsCollectionTestVersionsToAttach.getVersionName());
                attachedTestVersionsCollection.add(testVersionsCollectionTestVersionsToAttach);
            }
            testVersions.setTestVersionsCollection(attachedTestVersionsCollection);
            Collection<Protocols> attachedProtocolsCollection = new ArrayList<Protocols>();
            for (Protocols protocolsCollectionProtocolsToAttach : testVersions.getProtocolsCollection()) {
                protocolsCollectionProtocolsToAttach = em.getReference(protocolsCollectionProtocolsToAttach.getClass(), protocolsCollectionProtocolsToAttach.getId());
                attachedProtocolsCollection.add(protocolsCollectionProtocolsToAttach);
            }
            testVersions.setProtocolsCollection(attachedProtocolsCollection);
            em.persist(testVersions);
            if (parentVersion != null) {
                parentVersion.getTestVersionsCollection().add(testVersions);
                parentVersion = em.merge(parentVersion);
            }
            for (TestVersions testVersionsCollectionTestVersions : testVersions.getTestVersionsCollection()) {
                TestVersions oldParentVersionOfTestVersionsCollectionTestVersions = testVersionsCollectionTestVersions.getParentVersion();
                testVersionsCollectionTestVersions.setParentVersion(testVersions);
                testVersionsCollectionTestVersions = em.merge(testVersionsCollectionTestVersions);
                if (oldParentVersionOfTestVersionsCollectionTestVersions != null) {
                    oldParentVersionOfTestVersionsCollectionTestVersions.getTestVersionsCollection().remove(testVersionsCollectionTestVersions);
                    oldParentVersionOfTestVersionsCollectionTestVersions = em.merge(oldParentVersionOfTestVersionsCollectionTestVersions);
                }
            }
            for (Protocols protocolsCollectionProtocols : testVersions.getProtocolsCollection()) {
                TestVersions oldTestVersionOfProtocolsCollectionProtocols = protocolsCollectionProtocols.getTestVersion();
                protocolsCollectionProtocols.setTestVersion(testVersions);
                protocolsCollectionProtocols = em.merge(protocolsCollectionProtocols);
                if (oldTestVersionOfProtocolsCollectionProtocols != null) {
                    oldTestVersionOfProtocolsCollectionProtocols.getProtocolsCollection().remove(protocolsCollectionProtocols);
                    oldTestVersionOfProtocolsCollectionProtocols = em.merge(oldTestVersionOfProtocolsCollectionProtocols);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findTestVersions(testVersions.getVersionName()) != null) {
                throw new PreexistingEntityException("TestVersions " + testVersions + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TestVersions testVersions) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            TestVersions persistentTestVersions = em.find(TestVersions.class, testVersions.getVersionName());
            TestVersions parentVersionOld = persistentTestVersions.getParentVersion();
            TestVersions parentVersionNew = testVersions.getParentVersion();
            Collection<TestVersions> testVersionsCollectionOld = persistentTestVersions.getTestVersionsCollection();
            Collection<TestVersions> testVersionsCollectionNew = testVersions.getTestVersionsCollection();
            Collection<Protocols> protocolsCollectionOld = persistentTestVersions.getProtocolsCollection();
            Collection<Protocols> protocolsCollectionNew = testVersions.getProtocolsCollection();
            if (parentVersionNew != null) {
                parentVersionNew = em.getReference(parentVersionNew.getClass(), parentVersionNew.getVersionName());
                testVersions.setParentVersion(parentVersionNew);
            }
            Collection<TestVersions> attachedTestVersionsCollectionNew = new ArrayList<TestVersions>();
            for (TestVersions testVersionsCollectionNewTestVersionsToAttach : testVersionsCollectionNew) {
                testVersionsCollectionNewTestVersionsToAttach = em.getReference(testVersionsCollectionNewTestVersionsToAttach.getClass(), testVersionsCollectionNewTestVersionsToAttach.getVersionName());
                attachedTestVersionsCollectionNew.add(testVersionsCollectionNewTestVersionsToAttach);
            }
            testVersionsCollectionNew = attachedTestVersionsCollectionNew;
            testVersions.setTestVersionsCollection(testVersionsCollectionNew);
            Collection<Protocols> attachedProtocolsCollectionNew = new ArrayList<Protocols>();
            for (Protocols protocolsCollectionNewProtocolsToAttach : protocolsCollectionNew) {
                protocolsCollectionNewProtocolsToAttach = em.getReference(protocolsCollectionNewProtocolsToAttach.getClass(), protocolsCollectionNewProtocolsToAttach.getId());
                attachedProtocolsCollectionNew.add(protocolsCollectionNewProtocolsToAttach);
            }
            protocolsCollectionNew = attachedProtocolsCollectionNew;
            testVersions.setProtocolsCollection(protocolsCollectionNew);
            testVersions = em.merge(testVersions);
            if (parentVersionOld != null && !parentVersionOld.equals(parentVersionNew)) {
                parentVersionOld.getTestVersionsCollection().remove(testVersions);
                parentVersionOld = em.merge(parentVersionOld);
            }
            if (parentVersionNew != null && !parentVersionNew.equals(parentVersionOld)) {
                parentVersionNew.getTestVersionsCollection().add(testVersions);
                parentVersionNew = em.merge(parentVersionNew);
            }
            for (TestVersions testVersionsCollectionOldTestVersions : testVersionsCollectionOld) {
                if (!testVersionsCollectionNew.contains(testVersionsCollectionOldTestVersions)) {
                    testVersionsCollectionOldTestVersions.setParentVersion(null);
                    testVersionsCollectionOldTestVersions = em.merge(testVersionsCollectionOldTestVersions);
                }
            }
            for (TestVersions testVersionsCollectionNewTestVersions : testVersionsCollectionNew) {
                if (!testVersionsCollectionOld.contains(testVersionsCollectionNewTestVersions)) {
                    TestVersions oldParentVersionOfTestVersionsCollectionNewTestVersions = testVersionsCollectionNewTestVersions.getParentVersion();
                    testVersionsCollectionNewTestVersions.setParentVersion(testVersions);
                    testVersionsCollectionNewTestVersions = em.merge(testVersionsCollectionNewTestVersions);
                    if (oldParentVersionOfTestVersionsCollectionNewTestVersions != null && !oldParentVersionOfTestVersionsCollectionNewTestVersions.equals(testVersions)) {
                        oldParentVersionOfTestVersionsCollectionNewTestVersions.getTestVersionsCollection().remove(testVersionsCollectionNewTestVersions);
                        oldParentVersionOfTestVersionsCollectionNewTestVersions = em.merge(oldParentVersionOfTestVersionsCollectionNewTestVersions);
                    }
                }
            }
            for (Protocols protocolsCollectionOldProtocols : protocolsCollectionOld) {
                if (!protocolsCollectionNew.contains(protocolsCollectionOldProtocols)) {
                    protocolsCollectionOldProtocols.setTestVersion(null);
                    protocolsCollectionOldProtocols = em.merge(protocolsCollectionOldProtocols);
                }
            }
            for (Protocols protocolsCollectionNewProtocols : protocolsCollectionNew) {
                if (!protocolsCollectionOld.contains(protocolsCollectionNewProtocols)) {
                    TestVersions oldTestVersionOfProtocolsCollectionNewProtocols = protocolsCollectionNewProtocols.getTestVersion();
                    protocolsCollectionNewProtocols.setTestVersion(testVersions);
                    protocolsCollectionNewProtocols = em.merge(protocolsCollectionNewProtocols);
                    if (oldTestVersionOfProtocolsCollectionNewProtocols != null && !oldTestVersionOfProtocolsCollectionNewProtocols.equals(testVersions)) {
                        oldTestVersionOfProtocolsCollectionNewProtocols.getProtocolsCollection().remove(protocolsCollectionNewProtocols);
                        oldTestVersionOfProtocolsCollectionNewProtocols = em.merge(oldTestVersionOfProtocolsCollectionNewProtocols);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = testVersions.getVersionName();
                if (findTestVersions(id) == null) {
                    throw new NonexistentEntityException("The testVersions with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            TestVersions testVersions;
            try {
                testVersions = em.getReference(TestVersions.class, id);
                testVersions.getVersionName();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The testVersions with id " + id + " no longer exists.", enfe);
            }
            TestVersions parentVersion = testVersions.getParentVersion();
            if (parentVersion != null) {
                parentVersion.getTestVersionsCollection().remove(testVersions);
                parentVersion = em.merge(parentVersion);
            }
            Collection<TestVersions> testVersionsCollection = testVersions.getTestVersionsCollection();
            for (TestVersions testVersionsCollectionTestVersions : testVersionsCollection) {
                testVersionsCollectionTestVersions.setParentVersion(null);
                testVersionsCollectionTestVersions = em.merge(testVersionsCollectionTestVersions);
            }
            Collection<Protocols> protocolsCollection = testVersions.getProtocolsCollection();
            for (Protocols protocolsCollectionProtocols : protocolsCollection) {
                protocolsCollectionProtocols.setTestVersion(null);
                protocolsCollectionProtocols = em.merge(protocolsCollectionProtocols);
            }
            em.remove(testVersions);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TestVersions> findTestVersionsEntities() {
        return findTestVersionsEntities(true, -1, -1);
    }

    public List<TestVersions> findTestVersionsEntities(int maxResults, int firstResult) {
        return findTestVersionsEntities(false, maxResults, firstResult);
    }

    private List<TestVersions> findTestVersionsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from TestVersions as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TestVersions findTestVersions(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TestVersions.class, id);
        } finally {
            em.close();
        }
    }

    public int getTestVersionsCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from TestVersions as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
*/    
}
