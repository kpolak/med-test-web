package com.ge.med.medtestrest.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

/**
 * Test case definition entity.
 * User: Krzysztof Polak
 * Date: 20/11/13
 * Time: 15:17
 */
@Entity
@Table(name = "TEST_CASES")
@NamedQueries({
    @NamedQuery(name = "TestCase.findByCode", query = "SELECT tc FROM TestCase tc WHERE tc.testCode = :testCode"),
    @NamedQuery(name = "TestCase.findAll", query = "SELECT a FROM TestCase a")
})
@EqualsAndHashCode
@ToString
public class TestCase {

  @Id
  @Column(name = "TEST_CODE")
  @Getter
  @Setter
  private String testCode;

  @Column(name = "PRECONDITION", length = 4000)
  @Getter
  private String precondition;

  @Column(name = "TEST_INPUT", length = 4000)
  @Getter
  private String testInput;

  @Column(name = "TEST_OUTPUT", length = 4000)
  @Getter
  private String testOutput;

  //  @Column(name = "TEST_OUTPUT", length = 4000)
  @Transient
  @Getter
  private String userNote;

  public void setPrecondition(String precondition) {
    if (precondition != null && precondition.length() > 4000) {
      this.precondition = precondition.substring(0, 3999);
    } else {
      this.precondition = precondition;
    }
  }

  public void setTestInput(String testInput) {
    if (testInput != null && testInput.length() > 4000) {
      this.testInput = testInput.substring(0, 3999);
    } else {
      this.testInput = testInput;
    }
  }

  public void setTestOutput(String testOutput) {
    if (testOutput != null && testOutput.length() > 4000) {
      this.testOutput = testOutput.substring(0, 3999);
    } else {
      this.testOutput = testOutput;
    }
  }

  public void setUserNote(String userNote) {
    if (userNote != null && userNote.length() > 4000) {
      this.userNote = userNote.substring(0, 3999);
    } else {
      this.userNote = userNote;
    }
  }
}
