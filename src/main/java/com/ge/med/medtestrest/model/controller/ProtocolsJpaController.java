/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ge.med.medtestrest.model.controller;

import java.io.Serializable;
//import com.ge.med.medtestrest.model.TestVersions;
//import com.ge.med.medtestrest.model.ProtocolTestCases;
//import com.ge.med.medtestrest.model.Protocols;


/**
 *
 * @author kpolak
 */
public class ProtocolsJpaController implements Serializable {
/*
    public ProtocolsJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Protocols protocols) throws PreexistingEntityException, RollbackFailureException, Exception {
        if (protocols.getProtocolTestCasesCollection() == null) {
            protocols.setProtocolTestCasesCollection(new ArrayList<ProtocolTestCases>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            TestVersions testVersion = protocols.getTestVersion();
            if (testVersion != null) {
                testVersion = em.getReference(testVersion.getClass(), testVersion.getVersionName());
                protocols.setTestVersion(testVersion);
            }
            Collection<ProtocolTestCases> attachedProtocolTestCasesCollection = new ArrayList<ProtocolTestCases>();
            for (ProtocolTestCases protocolTestCasesCollectionProtocolTestCasesToAttach : protocols.getProtocolTestCasesCollection()) {
                protocolTestCasesCollectionProtocolTestCasesToAttach = em.getReference(protocolTestCasesCollectionProtocolTestCasesToAttach.getClass(), protocolTestCasesCollectionProtocolTestCasesToAttach.getId());
                attachedProtocolTestCasesCollection.add(protocolTestCasesCollectionProtocolTestCasesToAttach);
            }
            protocols.setProtocolTestCasesCollection(attachedProtocolTestCasesCollection);
            em.persist(protocols);
            if (testVersion != null) {
                testVersion.getProtocolsCollection().add(protocols);
                testVersion = em.merge(testVersion);
            }
            for (ProtocolTestCases protocolTestCasesCollectionProtocolTestCases : protocols.getProtocolTestCasesCollection()) {
                Protocols oldProtocolIdOfProtocolTestCasesCollectionProtocolTestCases = protocolTestCasesCollectionProtocolTestCases.getProtocolId();
                protocolTestCasesCollectionProtocolTestCases.setProtocolId(protocols);
                protocolTestCasesCollectionProtocolTestCases = em.merge(protocolTestCasesCollectionProtocolTestCases);
                if (oldProtocolIdOfProtocolTestCasesCollectionProtocolTestCases != null) {
                    oldProtocolIdOfProtocolTestCasesCollectionProtocolTestCases.getProtocolTestCasesCollection().remove(protocolTestCasesCollectionProtocolTestCases);
                    oldProtocolIdOfProtocolTestCasesCollectionProtocolTestCases = em.merge(oldProtocolIdOfProtocolTestCasesCollectionProtocolTestCases);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findProtocols(protocols.getId()) != null) {
                throw new PreexistingEntityException("Protocols " + protocols + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Protocols protocols) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Protocols persistentProtocols = em.find(Protocols.class, protocols.getId());
            TestVersions testVersionOld = persistentProtocols.getTestVersion();
            TestVersions testVersionNew = protocols.getTestVersion();
            Collection<ProtocolTestCases> protocolTestCasesCollectionOld = persistentProtocols.getProtocolTestCasesCollection();
            Collection<ProtocolTestCases> protocolTestCasesCollectionNew = protocols.getProtocolTestCasesCollection();
            if (testVersionNew != null) {
                testVersionNew = em.getReference(testVersionNew.getClass(), testVersionNew.getVersionName());
                protocols.setTestVersion(testVersionNew);
            }
            Collection<ProtocolTestCases> attachedProtocolTestCasesCollectionNew = new ArrayList<ProtocolTestCases>();
            for (ProtocolTestCases protocolTestCasesCollectionNewProtocolTestCasesToAttach : protocolTestCasesCollectionNew) {
                protocolTestCasesCollectionNewProtocolTestCasesToAttach = em.getReference(protocolTestCasesCollectionNewProtocolTestCasesToAttach.getClass(), protocolTestCasesCollectionNewProtocolTestCasesToAttach.getId());
                attachedProtocolTestCasesCollectionNew.add(protocolTestCasesCollectionNewProtocolTestCasesToAttach);
            }
            protocolTestCasesCollectionNew = attachedProtocolTestCasesCollectionNew;
            protocols.setProtocolTestCasesCollection(protocolTestCasesCollectionNew);
            protocols = em.merge(protocols);
            if (testVersionOld != null && !testVersionOld.equals(testVersionNew)) {
                testVersionOld.getProtocolsCollection().remove(protocols);
                testVersionOld = em.merge(testVersionOld);
            }
            if (testVersionNew != null && !testVersionNew.equals(testVersionOld)) {
                testVersionNew.getProtocolsCollection().add(protocols);
                testVersionNew = em.merge(testVersionNew);
            }
            for (ProtocolTestCases protocolTestCasesCollectionOldProtocolTestCases : protocolTestCasesCollectionOld) {
                if (!protocolTestCasesCollectionNew.contains(protocolTestCasesCollectionOldProtocolTestCases)) {
                    protocolTestCasesCollectionOldProtocolTestCases.setProtocolId(null);
                    protocolTestCasesCollectionOldProtocolTestCases = em.merge(protocolTestCasesCollectionOldProtocolTestCases);
                }
            }
            for (ProtocolTestCases protocolTestCasesCollectionNewProtocolTestCases : protocolTestCasesCollectionNew) {
                if (!protocolTestCasesCollectionOld.contains(protocolTestCasesCollectionNewProtocolTestCases)) {
                    Protocols oldProtocolIdOfProtocolTestCasesCollectionNewProtocolTestCases = protocolTestCasesCollectionNewProtocolTestCases.getProtocolId();
                    protocolTestCasesCollectionNewProtocolTestCases.setProtocolId(protocols);
                    protocolTestCasesCollectionNewProtocolTestCases = em.merge(protocolTestCasesCollectionNewProtocolTestCases);
                    if (oldProtocolIdOfProtocolTestCasesCollectionNewProtocolTestCases != null && !oldProtocolIdOfProtocolTestCasesCollectionNewProtocolTestCases.equals(protocols)) {
                        oldProtocolIdOfProtocolTestCasesCollectionNewProtocolTestCases.getProtocolTestCasesCollection().remove(protocolTestCasesCollectionNewProtocolTestCases);
                        oldProtocolIdOfProtocolTestCasesCollectionNewProtocolTestCases = em.merge(oldProtocolIdOfProtocolTestCasesCollectionNewProtocolTestCases);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = protocols.getId();
                if (findProtocols(id) == null) {
                    throw new NonexistentEntityException("The protocols with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Protocols protocols;
            try {
                protocols = em.getReference(Protocols.class, id);
                protocols.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The protocols with id " + id + " no longer exists.", enfe);
            }
            TestVersions testVersion = protocols.getTestVersion();
            if (testVersion != null) {
                testVersion.getProtocolsCollection().remove(protocols);
                testVersion = em.merge(testVersion);
            }
            Collection<ProtocolTestCases> protocolTestCasesCollection = protocols.getProtocolTestCasesCollection();
            for (ProtocolTestCases protocolTestCasesCollectionProtocolTestCases : protocolTestCasesCollection) {
                protocolTestCasesCollectionProtocolTestCases.setProtocolId(null);
                protocolTestCasesCollectionProtocolTestCases = em.merge(protocolTestCasesCollectionProtocolTestCases);
            }
            em.remove(protocols);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Protocols> findProtocolsEntities() {
        return findProtocolsEntities(true, -1, -1);
    }

    public List<Protocols> findProtocolsEntities(int maxResults, int firstResult) {
        return findProtocolsEntities(false, maxResults, firstResult);
    }

    private List<Protocols> findProtocolsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Protocols as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Protocols findProtocols(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Protocols.class, id);
        } finally {
            em.close();
        }
    }

    public int getProtocolsCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Protocols as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
*/    
}
