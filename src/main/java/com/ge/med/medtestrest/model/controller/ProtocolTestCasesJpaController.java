/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ge.med.medtestrest.model.controller;

//import com.ge.med.medtestrest.model.ProtocolTestCases;
import java.io.Serializable;
//import com.ge.med.medtestrest.model.Protocols;
//import com.ge.med.medtestrest.model.TestCases;


/**
 *
 * @author kpolak
 */
public class ProtocolTestCasesJpaController implements Serializable {
/*
    public ProtocolTestCasesJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ProtocolTestCases protocolTestCases) throws PreexistingEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Protocols protocolId = protocolTestCases.getProtocolId();
            if (protocolId != null) {
                protocolId = em.getReference(protocolId.getClass(), protocolId.getId());
                protocolTestCases.setProtocolId(protocolId);
            }
            TestCases testCode = protocolTestCases.getTestCode();
            if (testCode != null) {
                testCode = em.getReference(testCode.getClass(), testCode.getTestCode());
                protocolTestCases.setTestCode(testCode);
            }
            em.persist(protocolTestCases);
            if (protocolId != null) {
                protocolId.getProtocolTestCasesCollection().add(protocolTestCases);
                protocolId = em.merge(protocolId);
            }
            if (testCode != null) {
                testCode.getProtocolTestCasesCollection().add(protocolTestCases);
                testCode = em.merge(testCode);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findProtocolTestCases(protocolTestCases.getId()) != null) {
                throw new PreexistingEntityException("ProtocolTestCases " + protocolTestCases + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ProtocolTestCases protocolTestCases) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            ProtocolTestCases persistentProtocolTestCases = em.find(ProtocolTestCases.class, protocolTestCases.getId());
            Protocols protocolIdOld = persistentProtocolTestCases.getProtocolId();
            Protocols protocolIdNew = protocolTestCases.getProtocolId();
            TestCases testCodeOld = persistentProtocolTestCases.getTestCode();
            TestCases testCodeNew = protocolTestCases.getTestCode();
            if (protocolIdNew != null) {
                protocolIdNew = em.getReference(protocolIdNew.getClass(), protocolIdNew.getId());
                protocolTestCases.setProtocolId(protocolIdNew);
            }
            if (testCodeNew != null) {
                testCodeNew = em.getReference(testCodeNew.getClass(), testCodeNew.getTestCode());
                protocolTestCases.setTestCode(testCodeNew);
            }
            protocolTestCases = em.merge(protocolTestCases);
            if (protocolIdOld != null && !protocolIdOld.equals(protocolIdNew)) {
                protocolIdOld.getProtocolTestCasesCollection().remove(protocolTestCases);
                protocolIdOld = em.merge(protocolIdOld);
            }
            if (protocolIdNew != null && !protocolIdNew.equals(protocolIdOld)) {
                protocolIdNew.getProtocolTestCasesCollection().add(protocolTestCases);
                protocolIdNew = em.merge(protocolIdNew);
            }
            if (testCodeOld != null && !testCodeOld.equals(testCodeNew)) {
                testCodeOld.getProtocolTestCasesCollection().remove(protocolTestCases);
                testCodeOld = em.merge(testCodeOld);
            }
            if (testCodeNew != null && !testCodeNew.equals(testCodeOld)) {
                testCodeNew.getProtocolTestCasesCollection().add(protocolTestCases);
                testCodeNew = em.merge(testCodeNew);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = protocolTestCases.getId();
                if (findProtocolTestCases(id) == null) {
                    throw new NonexistentEntityException("The protocolTestCases with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            ProtocolTestCases protocolTestCases;
            try {
                protocolTestCases = em.getReference(ProtocolTestCases.class, id);
                protocolTestCases.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The protocolTestCases with id " + id + " no longer exists.", enfe);
            }
            Protocols protocolId = protocolTestCases.getProtocolId();
            if (protocolId != null) {
                protocolId.getProtocolTestCasesCollection().remove(protocolTestCases);
                protocolId = em.merge(protocolId);
            }
            TestCases testCode = protocolTestCases.getTestCode();
            if (testCode != null) {
                testCode.getProtocolTestCasesCollection().remove(protocolTestCases);
                testCode = em.merge(testCode);
            }
            em.remove(protocolTestCases);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ProtocolTestCases> findProtocolTestCasesEntities() {
        return findProtocolTestCasesEntities(true, -1, -1);
    }

    public List<ProtocolTestCases> findProtocolTestCasesEntities(int maxResults, int firstResult) {
        return findProtocolTestCasesEntities(false, maxResults, firstResult);
    }

    private List<ProtocolTestCases> findProtocolTestCasesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from ProtocolTestCases as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ProtocolTestCases findProtocolTestCases(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ProtocolTestCases.class, id);
        } finally {
            em.close();
        }
    }

    public int getProtocolTestCasesCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from ProtocolTestCases as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
*/    
}
